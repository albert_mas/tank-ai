﻿using UnityEngine;
using System.Collections;

public class KinematicSeek : MonoBehaviour {

	Move move;

	// Use this for initialization
	void Start () {
		move = GetComponent<Move>();
	}
	
	// Update is called once per frame
	void Update () 
	{
        // TODO 5: Set movement velocity to max speed in the direction of the target
        // https://math.stackexchange.com/questions/2062021/finding-the-angle-between-a-point-and-another-point-with-an-angle
        Vector3 vel = Vector3.zero;
        vel = (move.target.transform.position - move.aim.transform.position) * move.max_mov_velocity;
        move.SetMovementVelocity(vel);

        // Remember to enable this component in the inspector
    }
}
